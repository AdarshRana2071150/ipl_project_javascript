const DELIVERY_MATCH_ID = 0;
const DELIVERY_INNING = 1;
const DELIVERY_BOWLING_TEAM = 3;
const DELIVERY_OVER = 4;
const DELIVERY_BALL = 5;
const DELIVERY_BOWLER = 8;
const DELIVERY_EXTRA_RUNS = 16;
const DELIVERY_TOTAL_RUNS = 17;

const MATCHES_MATCH_ID = 0;
const MATCHES_SEASON = 1;
const MATCHES_WINNER = 10;

const iplForm = document.getElementById("iplForm");
const matchesFile = document.getElementById("matches");
const deliveriesFile = document.getElementById("deliveries");

var matches = new Array();
var deliveries = new Array();

const findNumberOfMatchesPlayedPerYear = () =>{
    const numberOfMatchesPlayedPerYear = new Map();

    for( const match of matches){
        if(!numberOfMatchesPlayedPerYear.has(match.season)){
            numberOfMatchesPlayedPerYear.set(match.season, 1);
        } else {
            numberOfMatchesPlayedPerYear.set(match.season, numberOfMatchesPlayedPerYear.get(match.season)+1);
        }
    }
    console.log("Number of matches played per year");
    for( let [season, numberOfMatches] of numberOfMatchesPlayedPerYear){
        console.log(season + " : "+ numberOfMatches);
    }
    console.log();
}

const findNumberOfMatchesWonPerTeam = () => {
    const numberOfMatchesWonPerTeam = new Map();

    for(const match of matches){
        if(!numberOfMatchesWonPerTeam.has(match.winner)){
            numberOfMatchesWonPerTeam.set(match.winner,1);
        } else {
            numberOfMatchesWonPerTeam.set(match.winner,numberOfMatchesWonPerTeam.get(match.winner)+1);
        }
    }
    console.log("Number of matches won per team");
    for( let [team, times] of numberOfMatchesWonPerTeam){
        console.log(team + " : "+ times);
    }
    console.log();
}

const findExtraRunsConcededPerTeamIn2016 = () => {
    var year = 2016;
    const matchIds2016 = new Set();
    const extraRunsConcededPerTeamIn2016 = new Map();

    for(const match of matches ){
        if(match.season == year){
            matchIds2016.add(match.id);
        }
    }
    for(const delivery of deliveries){
        if(matchIds2016.has(delivery.match_id)){
            if(!extraRunsConcededPerTeamIn2016.has(delivery.bowling_team)){
                extraRunsConcededPerTeamIn2016.set(delivery.bowling_team, Number(delivery.extra_runs));
            } else {
                extraRunsConcededPerTeamIn2016.set(delivery.bowling_team, extraRunsConcededPerTeamIn2016.get(delivery.bowling_team) + Number(delivery.extra_runs));
            }
        }
    }
    console.log("Extra runs conceded per team in 2016");
    for(let [team, extra_runs] of extraRunsConcededPerTeamIn2016){
        console.log("team: " + team + " : " + "extra_runs: " + extra_runs);
    }
    console.log();
}

const findMostEconomicalBowlerIn2015 = () => {
    var year = 2015;
    const matchIds2015 = new Set();
    const totalRunsByBowlerEachOver = new Map();

    for(const match of matches ){
        if(match.season == year){
            matchIds2015.add(match.id);
        }
    }

    for(var ball = 0; ball < deliveries.length; ball++){
        if(matchIds2015.has(deliveries[ball].match_id)){
            var firstBall = ball;
            var bowlerName = deliveries[ball].bowler ;
            var runs = 0;
            while( deliveries[firstBall].over == deliveries[ball].over){
                runs += Number(deliveries[ball].total_runs);
                ball++;
            }

            if(!totalRunsByBowlerEachOver.has(bowlerName)){
                totalRunsByBowlerEachOver.set(bowlerName, {over : 1, runsInOver : runs});
            } else {
                const bowlerStats = totalRunsByBowlerEachOver.get(bowlerName);
                const totalRuns = bowlerStats.runsInOver;
                const totalOvers = Number(bowlerStats.over);
                totalRunsByBowlerEachOver.set(bowlerName, {over : totalOvers+1, runsInOver : totalRuns + runs});
            }
        }
    }
    var economyOfBowlers = new Map();
    for( [bowler, bowlerStats] of totalRunsByBowlerEachOver){
        economyOfBowlers.set(bowler, Number(bowlerStats.runsInOver/bowlerStats.over));
    }
    const sortedEconomyOfBowlers = new Map([...economyOfBowlers.entries()].sort(
            (bowlerAndEconomyOne, bowlerAndEconomyTwo) => bowlerAndEconomyOne[1] - bowlerAndEconomyTwo[1])
        );
    console.log("Economy of bowlers in 2015");
    for(let [bowler, economy] of sortedEconomyOfBowlers){
        console.log("bowler: " + bowler + " : " + "economy: " + economy);
    }
    console.log();
}

const findHighestScoreOfAllTimesInOneInnings = () =>{
    const maxRunPerMatch = new Map();
    var maxScore = 0;

    for(var ball = 0; ball < deliveries.length; ball++){
        var currentMatchId = deliveries[ball].match_id;
        var firstInningScore = 0;
        var secondInningScore = 0;

        while(ball < deliveries.length && currentMatchId == deliveries[ball].match_id){
            if( deliveries[ball].inning == 1){
                firstInningScore += Number(deliveries[ball].total_runs);
            } else if (deliveries[ball].inning == 2){
                secondInningScore += Number(deliveries[ball].total_runs);
            }
            ball++;
        }
        ball--;
        maxRunPerMatch.set(currentMatchId, Math.max(firstInningScore, secondInningScore));
    }
    for(let [matchId, highestScore] of maxRunPerMatch){
        if(maxScore < highestScore){
            maxScore = highestScore;
        }
    }
    console.log("Maximum Score of all times\n");
    console.log(maxScore);
}

document.getElementById("btn").addEventListener("click",(e)=>{
    e.preventDefault();
    findNumberOfMatchesPlayedPerYear();
    findNumberOfMatchesWonPerTeam();
    findExtraRunsConcededPerTeamIn2016();
    findMostEconomicalBowlerIn2015();
    findHighestScoreOfAllTimesInOneInnings();
})

document.getElementById("matches").addEventListener("change", function(event){
    const inputMatchesFile = matchesFile.files[0];
    const reader = new FileReader();

    reader.readAsText(inputMatchesFile);
    reader.onload = function(event) {
        const text = event.target.result;
        const rows = text.slice(text.indexOf("\n") + 1).split("\n");
        matches = rows.map(function (row) {
            const values = row.split(",");
            return {
                id: values[MATCHES_MATCH_ID], season: values[MATCHES_SEASON], winner: values[MATCHES_WINNER]
            };
        });
    };
});

document.getElementById("deliveries").addEventListener("change", function(event){
    const inputDeliveriesFile = deliveriesFile.files[0];
    const reader = new FileReader();

    reader.readAsText(inputDeliveriesFile);
    reader.onload = function(event) {
        const text = event.target.result;
        const rows = text.slice(text.indexOf("\n") + 1).split("\n");
        deliveries = rows.map(function (row) {
            const values = row.split(",");
            return {
                match_id: values[DELIVERY_MATCH_ID], inning: values[DELIVERY_INNING], bowling_team: values[DELIVERY_BOWLING_TEAM],
                over: values[DELIVERY_OVER], ball: values[DELIVERY_BALL], bowler: values[DELIVERY_BOWLER],
                extra_runs: values[DELIVERY_EXTRA_RUNS], total_runs: values[DELIVERY_TOTAL_RUNS]
            };
        });
    };
});

